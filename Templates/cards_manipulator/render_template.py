import argparse     # parsing parameters
import os
import jinja2       # generating template
import json     # working with json files
import shutil       # file/directory operations
import glob     # file paths matching
from subprocess import run      # executing external commands
from glob import glob       # simpler file operations
from PIL import Image       # slicing images
from fpdf import FPDF       # generating pdfs
import time     # monitoring execution time


def prepare_work_directory(path):
	""" Prepare new directory to store produced files

	Construct path based on the location of the template HTML.
	Clear or create directory for the files that will be produced during PDF generation.

	:param path: path to the template HTML
	:return: path of the created work directory
	"""

	work_directory = os.path.join(path, 'cardrenderer_temp')
	# Remove folder if it already exists
	if os.path.exists(work_directory):
		try:
			shutil.rmtree(work_directory)
		except Exception as e:
			print(e)
	os.mkdir(work_directory)     # recreate directory
	os.mkdir(os.path.join(work_directory, 'render'))    # directory for rendered htmls
	os.chdir(work_directory)     # move inside by making it current working directory
	return work_directory


def process_template(template_path, data_path):
	""" Fill template with data from data-set

	Process template (HTML) file. Create folder 'render' in current work directory.
	Save filled with data html files in created folder.

	:param template_path: Path to the template HTML
	:param data_path: Path to the data-set json file
	:return:
	"""

	# specify directory path to template
	env = jinja2.Environment(
		loader=jinja2.FileSystemLoader(os.path.dirname(template_path))
	)
	# environment is already in the folder with the template
	template = env.get_template(os.path.basename(template_path))      # read json file
	try:
		with open(data_path) as file:
			data = json.load(file)
			# generate every card separately
			for index, item in enumerate(data['set']):
				outputfile_path = 'render/{0:06}.html'.format(index)
				output = open(outputfile_path, 'w')
				render = template.render(set=[item, ])
				output.write(render)     # dummy array to render one card
				output.close()      # save file to disk
				print('.', end='', flush=True)
			print('Done')     # a new line after the dots
	except Exception as e:
		print("Error processing the file. Verify file name and try again.")
		print(e)


def generate_style(template_path):
	""" Generate stylesheet CSS file out of SCSS files

	Generates CSS filenames and consequently generates CSS out of SCSS.

	Partials (SCSS files starting with underscore) are skipped as they should not be compiled alone.

	:param template_path: Path the the template HTML
	:return:
	"""

	rootdir = os.path.dirname(template_path)
	resource_files = []
	resource_files.extend(glob(os.path.join(rootdir, '*.scss')))
	for file in resource_files:
		# SCSS files starting with underscore are modules and the should not be compiled on their own
		if os.path.basename(file).startswith('_'):
			print('Skipping partial SCSS:', file)
			continue
		output_file = file.replace('.scss', '.css')
		run([shutil.which('sass'), file, output_file])      # subprocess.run does not check the PATH


def copy_resources(template_path):
	""" Copy graphics folder, stylesheets, and scripts to working directory

	Copy graphics files, scripts, and stylesheets that are lying next to the template to ensure it renders correctly.

	:param template_path: Path to the template HTML
	:return:
	"""

	try:
		graphics_dir = os.path.dirname(template_path)
		graphics_dir = os.path.join(graphics_dir, 'graphics')
		shutil.copytree(graphics_dir, 'graphics')
	except Exception as e:
		print('Error while copying graphics')
		print(e)
	# style files and scripts
	# gather list of files to copy
	required_extensions = ['*.css', '*.js']
	resource_files = []
	for extension in required_extensions:
		resource_files.extend(glob(os.path.join(os.path.dirname(template_path), extension)))
	# copy file in current working directory
	for file in resource_files:
		shutil.copy(file, os.path.join(os.getcwd(), 'render'))


def render_html(tool_path, width, height):
	""" Process template html with slimerjs -> firefox 52

	Render each product with Firefox controlled by slimerjs.

	**Headless mode of new Firefox may be used.**

	:param tool_path: Path to the directory with the tools
	:param width: Width of the rendered template in px
	:param height: Height of the rendered template in px
	:return: 
	"""
	tool = 'slimerjs-0.10.3'
	# render html to pngs
	os.environ['SLIMERJSLAUNCHER'] = str('C:/Program Files/Mozilla Firefox/firefox.exe')
	files_to_render = glob(os.path.join(os.getcwd(), 'render', '*.html'))
	size = '"{0}px*{1}px"'.format(width, height)
	for file in files_to_render:
		command = os.path.join(tool_path, tool, 'slimerjs.bat')
		subcommand = os.path.join(tool_path, tool, 'rasterize.js')
		subcommand = os.path.abspath(subcommand)        # fixing path (Error: script not found)
		input_file = 'file:///{}'.format(file).replace('\\', '/')       # URI paths does not contain backslashes
		filename = os.path.splitext(os.path.basename(file))[0]      # get just the name of the file
		output_file = os.path.join(os.getcwd(), 'render', '{}.png'.format(filename))
		print('.', end='', flush=True)
		run([command, subcommand, input_file, output_file, size])
	print('Done')     # a new line after the dots


def split_images(direction):        # todo: implement horizontal/vertical split
	""" Split rendered images into halves

	:param direction: Direction of the cut (*not yet implemented*)
	:return: List of slices (sides of the products)
	"""
	# split images into pieces
	images_to_split = glob(os.path.join(os.getcwd(), 'render', '*.png'))
	slices = list()
	for image in images_to_split:
		source_image = Image.open(image)
		width, height = source_image.size
		upper = 0
		for slice in range(2):
			side = height / 2
			if slice != 0:
				upper = side
				side = height
			bbox = (0, upper, width, side)
			w_slice = source_image.crop(bbox)
			slice_file = os.path.join(os.getcwd(), 'render', 'slice_{}_{}.png'.format(os.path.splitext(os.path.basename(image))[0], slice))
			w_slice.save(slice_file)
			slices.append(slice_file)
			print('.', end='', flush=True)
	print('Done')       # a new line after the dots

	return slices


def render_pdf(pages, width, height):
	""" Create final PDF out of sliced files

	Saves **render.pdf** file on the disk next to template HTML.

	:param pages: List of slices that will be added as separate pages
	:param width: Width of the PDF in mm
	:param height: Height of the PDF in mm
	:return:
	"""
	pdf = FPDF('P', 'mm', (width, height))
	for image in pages:
		pdf.add_page()
		file = open(image)
		pdf.image(image, 0, 0, width, height, 'png', pdf.add_link())
		print('.', end='', flush=True)
	pdf.output('render.pdf', 'F')
	print('Done')       # a new line after the dots


def check_environment():
	""" Check if the environment has all the required tools

	:return: **True** if all the programs are accessible
	**False** if some applications are missing
	"""

	tools = ['ruby', 'sass']

	for tool in tools:
		tool_path = shutil.which(tool)      # we are happy with any version right now
		if not tool_path:
			print(tool, 'is missing. Install it first to continue.')
			return False    # tool is missing

	return True     # can continue


if __name__ == "__main__":
	# exit if environment is not ready
	if not check_environment():
		exit(1)

	# Creating arguments section
	parser = argparse.ArgumentParser()
	parser.add_argument('template', help='Input HTML file for processing.\n'
'File with style (CSS), scripts (JS), and graphics in folder named graphics should be located in the same folder.')
	parser.add_argument('data', help='Input JSON file containing set for processing.')
	parser.add_argument('tools', help='Folder containing slimerjs and other tools.')
	parser.add_argument('width_html', help='Width of the original render in pixels')
	parser.add_argument('height_html', help='Height of the original render in pixels')
	parser.add_argument('width_mm', help='Width of the output render in mm')
	parser.add_argument('height_mm', help='Height of the output render in mm')
	arguments = parser.parse_args()     # now they will be accessible as arguments.___

	start_time = time.time()
	work_directory = os.path.dirname(arguments.template)        # get directory name
	print('Preparing working directory')
	work_directory = prepare_work_directory(work_directory)     # set up workspace

	template_start_time = time.time()
	print('Processing HTML template')
	process_template(arguments.template, arguments.data)        # filling template with data
	template_stop_time = time.time()

	print('Prepare rendering environment')
	generate_style(arguments.template)
	copy_resources(arguments.template)

	webrender_start_time = time.time()
	print('Rendering HTML to images.')
	try:
		width_html = int(arguments.width_html)
		height_html = int(arguments.height_html)
	except:
		print('Sizes in px are not numbers')
		exit(1)
	render_html(arguments.tools, width_html, height_html)
	webrender_stop_time = time.time()

	images_start_time = time.time()
	print('Splitting pages into parts')
	pages = split_images(0)
	images_stop_time = time.time()

	pdf_start_time = time.time()
	print('Rendering pdf')
	try:
		width_mm = float(arguments.width_mm)
		height_mm = float(arguments.height_mm)
	except:
		print('Sizes in mm are not numbers')
		exit(1)
	render_pdf(pages, width_mm, height_mm)
	pdf_stop_time = time.time()

	elapsed_time = round(time.time() - start_time, 2)
	# template_time
	webrender_time = round(webrender_stop_time - webrender_start_time, 2)
	# images_time
	pdf_time = round(pdf_stop_time - pdf_start_time, 2)
	print('\nTime elapsed:', elapsed_time, 's', '({:.3}min)'.format(elapsed_time / 60))
	print('- Template processing:', round(template_stop_time - template_start_time, 2), 's')
	print('- WEB rendering:', webrender_time, 's', '({:.3}min)'.format(webrender_time / 60))
	print('- Image processing:', round(images_stop_time - images_start_time, 2), 's')
	print('- PDF generation:', pdf_time, 's', '({:.3}min)'.format(pdf_time / 60))

