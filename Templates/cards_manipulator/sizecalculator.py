# project: cards
# author: Rustam Rahimgulov
# date: 16.03.2018
# version 0.0.2

html_dpi = 96       # default browser printing density
target_dpi = 300    # default printer density


def set_target_dpi(dpi):
	global target_dpi
	target_dpi = dpi


def mm2px(mm, dpi):
	px = round(mm / 25.4 * dpi)
	return px


def px2mm(px, dpi):
	mm = round(25.4 * px / dpi, 3)
	return mm


def pt2px(pt, dpi):
	px = round(dpi * pt / 72)
	return px


def extract_number(str, breakpoint):
	return str[:str.find(breakpoint)]


if __name__ == "__main__":
	dpiupdate = input('Update target dpi? Current is {0}. Enter new value: '.format(target_dpi))
	if dpiupdate:
		target_dpi = int(dpiupdate)
	while True:
		command = input('enter a number in mm, pt or new target dpi: ')
		if command.endswith('dpi'):
			set_target_dpi(int(extract_number(command, 'dpi')))
			continue
		elif command.endswith('mm'):
			command = float(extract_number(command, 'mm'))
			print('{0}px with {1}dpi'.format(mm2px(command, target_dpi), target_dpi))
		elif command.endswith('pt'):
			command = float(extract_number(command, 'pt'))
			print('{0}px with {1} target dpi'.format(pt2px(command, target_dpi), target_dpi))
		elif command.lower() == 'exit':
			break
