# render_template.py

Accepts multiple parameters. All of them are mandatory for the script to operate.

    data tools width_html height_html width_mm height_mm

`data` - path to HTML template

`tools` - path to directory with tools (slimerjs 0.10.3)

`width_/heigth_html` - size of the rendered template in pixels

`widht_/heigth_mm` - size of the resulting printed page in millimeters

Pages are split horizontally in half and then added to PDF.

Requires several additional python packages. Can be installed by using corresponding `_requirements.txt` file.

> Is vertical splitting needed?

## Issues

* Parameters are not processed with type/illegal characters checking. Error-prone!
* Very basic check for installed software. Versions are not checked.
