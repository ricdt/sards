# project: cards
# author: Rustam Rahimgulov
# date: 27.12.2017
# version 0.0.1

""" seteditor.py

Script provides an interactive way for user to verify and update data used for inflating the
template.

:param Input_file: file with JSON to process
:param Output_file: file to save changed data

:return: Saves modified file to disk
"""

from classes.Entry import Entry
from classes.Set import Set

import sys		# command line arguments
import os.path		# check if file exists

input_file = ''		# File
output_file = ''
parent_container = ''


def get_cmd_arguments():
	""" Verify correctness of the parameters passed.

	Verify if the parameters are correct and file exists. Otherwise, print the synopsis
	and exit.
	"""
	global input_file, output_file
	# sys.argv[0] - script name
	# sys.argv[1] - processed file
	# check if proper number of parameters has been passed
	if len(sys.argv) == 1 or len(sys.argv) > 3:
		print("Specify parameters. Refer to script synopsis and try again.")
		sys.exit(1)
	# check if file exists
	input_file = sys.argv[1]
	if not os.path.exists(input_file):
		print("Specified file does not exist.")
		exit(1)
	# check if output filename was entered. If it's not use default one
	if (len(sys.argv) >= 3) and sys.argv[2]:
		output_file = "".join(x for x in sys.argv[2] if (x.isalnum() or x in "._- "))
	else:
		output_file = os.path.splitext(os.path.basename(sys.argv[1]))[0] + "_modified.json"


# Description of the available commands
commands = {
	"synopsis": "print welcoming message",
	"count": "gets number of entries in the current loaded file",
	"list": "list files from the working set",
	"get": "select object to work with",
	"delete": "delete object from the working set",
	"add": "adds new Entry object to list",

	"help": "displays this help"
}

current_object = None   # object we are working with
working_set = Set()


def synopsis(*args):
	""" Print synopsis (how to use the script) """
	print('To exit script type "exit"')


def list_entries(indices):
	""" List entries all or in a specified range

	@param indices Indices of firs and last - 1 entry to show
	"""
	# todo: work with indexing as it may lead to many exceptions and bugs
	for item in working_set.list(int(input("Start index: ")), int(input("End index: "))):   # todo: get the parameter; get rid of `input()`
		print(item)


def list_commands(*args):       # todo: list help for a selected command
	for command, description in commands.items():
		print("{0} - {1}".format(command, description))


def get_object(index):
	global current_object
	try:
		if not index:
			index = int(input("Index of the object: "))
		current_object = working_set.get_entry(int(index))
		print("Object selected!\n", current_object)
	except TypeError as e:
		print("wtf", e)


def add_entry_field(filed_name):
	global current_object
	try:
		current_object.add_edit_field(filed_name, input("Enter content of the new field: "))
	except:
		print("Have you selected the entry first?")


def delete_from_set(*args):
	working_set.remove_entry(current_object)


def add_to_list(*args):
	global current_object
	current_object = Entry({})      # empty object
	working_set.add_entry(current_object)


if __name__ == "__main__":
	get_cmd_arguments()
	working_set.load_from_file()    # data is loaded and processed

	user_input = 'synopsis'
	options = {
		"synopsis": synopsis,
		"count": working_set.count,
		"list": list_entries,
		"get": get_object,
		"delete": delete_from_set,
		"add field": add_entry_field,
		"add": add_to_list,

		"help": list_commands
	}
	params = ''     # variable to store parameters
	while user_input != "exit":
		# check if the command is in the list of available commands
		if user_input in options.keys():
			options[user_input](params)
		else:
			print("Unknown command.")
		user_input_t = input("> ").lower().split(' ', 1)      # split at space only once
		# handle situation when command had a parameter
		if len(user_input_t) > 1:
			user_input = user_input_t[0].strip()
			params = user_input_t[1].strip()        # split happened only once
		else:
			user_input = user_input_t[0]
	if input("Do you want to save the file? [y/n]\nPath: {0}\n".format(output_file)) == "y":
		working_set.save_to_file(output_file)
