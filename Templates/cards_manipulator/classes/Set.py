from .Entry import Entry

import sys
import json


class Set:
	""" Class to contain Entry objects

	To allow adequate interactions with the data
	1. CRUD access
	2. Random access
	3. Search
	4. Statistics
	"""
	def __init__(self):
		self.entry_list = []

	def add_entry(self, entry_object):
		if entry_object not in self.entry_list:
			self.entry_list.append(entry_object)

	def get_entry(self, index):
		return self.entry_list[index]   # todo: boundary checks needed!

	def remove_entry(self, entry_object):
		""" Remove object from the set """
		self.entry_list.remove(entry_object)

	def count(self):
		return len(self.entry_list)

	def list(self, start, stop):
		rlist = []
		for index in range(start, stop):
			rlist.append(self.get_entry(index))
		return rlist

	def load_from_file(self, file_path):
		try:
			with open(file_path) as file:
				raw_data = json.load(file)
		except Exception as e:
			print("Error processing the file. Verify file name and try again.")
			print("File cannot be opened. Verify that file exists and readable and try again.")
			sys.exit(1)

		# iter() and next() are hack because we always have only one parent node "items"
		global parent_container
		parent_container = raw_data.keys().__iter__().__next__()
		for entry in raw_data[parent_container]:
			self.add_entry(Entry(entry))

	def save_to_file(self, file_path):       # fixme: not debugged yet
		""" save when everything is changed """
		try:
			output_file = open(file_path, "w+")
			list_json = {parent_container: list()}
			for entry in self.entry_list:
				list_json[parent_container].append(entry.to_json())
			json.dump(list_json, output_file)
		except Exception as e:
			print("Exception while handling files.")
			exit(1)
