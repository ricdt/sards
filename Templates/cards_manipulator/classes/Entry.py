import io


class Entry:
	""" Class to represent entry in JSON data file

	To allow adequate interactions with the data
	1. CRUD access
	2. Random access
	3. Search
	4. Statistics
	"""
	def __init__(self, entry_dict):
		""" Constructor parsing processed dictionary.

		Entry is stored in JSON and will be converted to dictionary by JSON lib. Default JSONEncoder
		is user. Thus, type should be properly parsed. dictionary expected.

		:param entry_dict
		"""
		self.object = entry_dict

	def list_fields(self):
		for key, value in self.object.items():
			print("{0}: {1}".format(key, value))

	def add_edit_field(self, key, value):
		"""

		:param key
		:param value

		:return:
		"""
		# todo: limit and check input
		self.object[key] = value    # todo: is any check needed?

	def remove_field(self, key):
		try:
			self.object.remove(key)
		except Exception as e:
			print('Key does not exist in the object ', e.__class__())

	def __repr__(self):
		# reasonably effective string manipulation
		# https://docs.python.org/3/library/io.html#io.StringIO
		rstring = io.StringIO()
		for key, value in self.object.items():
			rstring.write("{0}: {1}\n".format(key, value))
		return rstring.getvalue()

	def to_json(self):
		return self.object
