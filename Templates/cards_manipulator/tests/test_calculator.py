import unittest
from random import randint

import cards_manipulator.sizecalculator as calculator


class CalculatorTest(unittest.TestCase):
	def setUp(self):
		self.customdpi = randint(1, 3000)
		self.sizes_mm = self.sizes_pt = [-210, -10, -5.5, -3.125, -1, -0.001, 0, 0.001, 2, 4.987, 6.2, 11, 220]
		self.sizes_px = [-444, -150, -13, -12, -1, 0, 12, 13, 15, 201]
		for i in range(3):
			tvar = randint(-2000, 2000)
			self.sizes_mm.append(tvar)
			self.sizes_pt.append(tvar)
			self.sizes_px.append(tvar)

	def tearDown(self):
		pass

	def calculate_px(self, ratio):
		sizes = list()
		for size in self.sizes_mm:
			sizes.append(round(size * ratio))
		return sizes

	def test_mm2px_default_dpi(self):
		sizes_px_300 = self.calculate_px(11.811)

		for size in self.sizes_mm:
			with self.subTest(size):
				px = calculator.mm2px(size, 300)
				self.assertEqual(sizes_px_300[self.sizes_mm.index(size)], px, 'Sizes in px do not match')

	def test_mm2px_random_dpi(self):
		sizes_custom = self.calculate_px(self.customdpi / 25.4)

		for size in self.sizes_mm:
			with self.subTest(size):
				px = calculator.mm2px(size, self.customdpi)
				self.assertEqual(sizes_custom[self.sizes_mm.index(size)], px, 'Sizes in px do not match, with dpi={0}'.format(self.customdpi))

	def test_px2mm_default_dpi(self):
		for size in self.sizes_px:
			with self.subTest(size):
				mm = calculator.px2mm(size, 96)
				self.assertEqual(round(size * 25.4 / 96, 3), mm, 'Sizes in px and mm do not match')

	def test_px2mm_custom_dpi(self):
		for size in self.sizes_px:
			with self.subTest(size):
				mm = calculator.px2mm(size, self.customdpi)
				self.assertEqual(round(size * 25.4 / self.customdpi, 3), mm, 'Sizes in px and mm do not match')

	def test_pt2px_96dpi(self):
		for size in self.sizes_pt:
			with self.subTest(size):
				px = calculator.pt2px(size, 96)
				self.assertEqual(round(1.3334 * size), calculator.pt2px(size, 96), 'Sizes in px do not match')

	def test_pt2px_random_dpi(self):
		sizes_custom = self.calculate_px(self.customdpi / 72)

		for size in self.sizes_mm:
			with self.subTest(size):
				px = calculator.pt2px(size, self.customdpi)
				self.assertEqual(sizes_custom[self.sizes_mm.index(size)], px, 'Sizes in px do not match, with dpi={0}'.format(self.customdpi))
